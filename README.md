Install the "node.js", "git" and "vscode".
Open the "vscode". 
Open Terminal in "vscode".
change to the directory in which you want the save the work.
copy the clone link href "https://github.com/rhildred/es6test.git".
run the clone link href as "https://github.com/rhildred/es6test.git ." in the terminal in working directory.
open the required ".js" file and work on it and once it is done to run the code, press control + f5.

Note : I used "GNU General Public License (GPL) version 3" because 1) Its freely available. 2)Latest GPL available version.
